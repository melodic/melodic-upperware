<!--
  ~ Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
  ~
  ~ This Source Code Form is subject to the terms of the Mozilla Public
  ~ License, v. 2.0. If a copy of the MPL was not distributed with this file,
  ~ You can obtain one at http://mozilla.org/MPL/2.0/.
  ~
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.0.RELEASE</version>
    </parent>

    <groupId>org.ow2.paasage</groupId>
    <artifactId>upperware</artifactId>
    <version>3.1.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>Upperware</name>

    <modules>
        <module>upperware_metamodel</module>
        <module>jwt-commons</module>
        <module>jwt-server</module>
        <module>melodic-commons</module>
        <module>melodic-cache</module>
        <module>adapter</module>
        <module>cp_generator</module>
        <module>gui-backend</module>
        <module>meta_solver</module>
        <module>utility-generator</module>
        <module>cp-solver</module>
        <module>la_converter</module>
        <module>dlms</module>
        <module>event-management</module>
        <module>mq-http-adapter</module>
        <module>penalty-calculator</module>
        <module>zpp-solver</module>
        <module>functionizer-testing-tool</module>
    </modules>


    <properties>
        <melodic.version>3.1.0-SNAPSHOT</melodic.version>

        <!-- version for all paasage components-->
        <camel.version>3.1.0-SNAPSHOT</camel.version>

        <interfaces.version>3.1.0-SNAPSHOT</interfaces.version>

        <!-- Source encoding -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- Versions for common dependencies -->
        <!-- Unused at the moment -->
        <jeromq.version>0.3.5</jeromq.version>
        <junit.version>4.12</junit.version>
        <log4j.version>1.2.17</log4j.version>
        <commons-lang3.version>3.6</commons-lang3.version>
        <commons-collections4.version>4.1</commons-collections4.version>

        <!-- Versions for common plugins -->
        <source-plugin.version>2.4</source-plugin.version>
        <maven-compiler.version>3.2</maven-compiler.version>
        <javadoc-plugin.version>2.9.1</javadoc-plugin.version>
        <maven-assembly-plugin.version>2.5.3</maven-assembly-plugin.version>

        <!--DOCKER plugin properties-->
        <docker.push>false</docker.push>
        <docker.repository>127.0.0.1:5000/</docker.repository>
        <!--suppress UnresolvedMavenProperty -->
        <docker.imageTag>${scmBranch}</docker.imageTag>
        <docker.imagePrefix>melodic/melodic-upperware/</docker.imagePrefix>
        <docker.imageName>parent</docker.imageName>
        <docker.jarToInclude>${project.build.finalName}.jar</docker.jarToInclude>
        <docker.spotify-plugin.version>0.4.10</docker.spotify-plugin.version>
    </properties>


    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.melodic</groupId>
                <artifactId>interfaces</artifactId>
                <version>${interfaces.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>eu.melodic</groupId>
                        <artifactId>jaxrs-code-generator-7bulls</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>org.ow2.paasage</groupId>
                <artifactId>melodic-commons</artifactId>
                <version>${melodic.version}</version>
            </dependency>
            <dependency>
                <groupId>org.ow2.paasage</groupId>
                <artifactId>upperware-metamodel</artifactId>
                <version>${melodic.version}</version>
            </dependency>
            <dependency>
                <groupId>org.ow2.paasage</groupId>
                <artifactId>jwt-commons</artifactId>
                <version>${melodic.version}</version>
            </dependency>
            <dependency>
                <groupId>org.ow2.paasage.mddb.cdo</groupId>
                <artifactId>client</artifactId>
                <!-- This may be strange but is necessary -
                while adding dependency in child pom ${parent.version}
                will be set to upperware version not parent of upperware -->
                <!--<version>${parent.version}</version>-->
                <version>3.1.0-SNAPSHOT</version>
            </dependency>
            <dependency>
                <groupId>eu.melodic.cdo</groupId>
                <artifactId>client</artifactId>
                <version>3.1.0-SNAPSHOT</version>
                <classifier>repackaged</classifier>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-collections4</artifactId>
                <version>${commons-collections4.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons-lang3.version}</version>
            </dependency>

            <dependency>
                <groupId>camel</groupId>
                <artifactId>camel</artifactId>
                <version>${camel.version}</version>
            </dependency>
            <dependency>
                <groupId>camel</groupId>
                <artifactId>camel.dsl</artifactId>
                <version>${camel.version}</version>
            </dependency>

            <dependency>
                <groupId>io.github.cloudiator.client</groupId>
                <artifactId>java-rest</artifactId>
                <version>0.3.0-SNAPSHOT</version>
                <scope>compile</scope>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>${source-plugin.version}</version>
                    <inherited>true</inherited>
                    <executions>
                        <execution>
                            <id>attach-sources</id>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven-compiler.version}</version>
                    <inherited>true</inherited>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${javadoc-plugin.version}</version>
                    <executions>
                        <execution>
                            <id>aggregate</id>
                            <goals>
                                <goal>aggregate</goal>
                            </goals>
                            <phase>package</phase>
                            <configuration>
                                <additionalparam>-Xdoclint:none</additionalparam>
                            </configuration>
                        </execution>
                        <execution>
                            <id>attach-javadocs</id>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                            <configuration>
                                <additionalparam>-Xdoclint:none</additionalparam>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>2.5</version>
                    <configuration>
                        <autoVersionSubmodules>true</autoVersionSubmodules>
                        <tagNameFormat>v@{project.version}</tagNameFormat>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>${docker.spotify-plugin.version}</version>
                    <configuration>
                        <dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
                        <imageName>${docker.repository}${docker.imagePrefix}${docker.imageName}:${docker.imageTag}</imageName>
                        <pushImage>${docker.push}</pushImage>
                        <serverId>ow2.melodic</serverId>
                        <resources>
                            <resource>
                                <targetPath>/</targetPath>
                                <directory>${project.build.directory}</directory>
                                <include>${docker.jarToInclude}</include>
                            </resource>
                        </resources>
                    </configuration>
                    <executions>
                        <execution>
                            <id>execution</id>
                            <phase>install</phase>
                            <goals>
                                <goal>build</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <!--plugins for getting scmBranch value lowercase to use in docker images -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>buildnumber-maven-plugin</artifactId>
                    <version>1.4</version>
                    <executions>
                        <execution>
                            <phase>validate</phase>
                            <goals>
                                <goal>create</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.groovy.maven</groupId>
                    <artifactId>gmaven-plugin</artifactId>
                    <version>1.0</version>
                    <executions>
                        <execution>
                            <phase>initialize</phase>
                            <goals>
                                <goal>execute</goal>
                            </goals>
                            <configuration>
                                <source>
                                    import org.apache.commons.lang.StringUtils

                                    project.properties["scmBranch"] = StringUtils.lowerCase(project.properties["scmBranch"])
                                </source>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>2.8.1</version>
                    <executions>
                        <execution>
                            <id>default-deploy</id>
                            <phase>deploy</phase>
                            <goals>
                                <goal>deploy</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
    <repositories>
        <repository>
            <id>eu.7bulls</id>
            <name>Melodic 7bulls repository</name>
            <url>https://nexus.7bulls.eu:8443/repository/maven-public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>

        <repository>
            <id>maven-central</id>
            <url>https://repo1.maven.org/maven2/</url>
        </repository>

        <repository>
            <id>ow2</id>
            <name>OW2 repository</name>
            <url>http://repository.ow2.org/nexus/content/repositories/public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>paasage.repo</id>
            <name>PaaSage repository</name>
            <url>http://jenkins.paasage.cetic.be/repository/</url>
        </repository>
        <repository>
            <id>ossrh.snapshots</id>
            <name>OSSRH snapshots repository</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
    <distributionManagement>
        <repository>
            <id>eu.7bulls</id>
            <name>Melodic 7bulls repository</name>
            <url>https://nexus.7bulls.eu:8443/repository/maven-releases/</url>
        </repository>
        <snapshotRepository>
            <id>eu.7bulls</id>
            <name>Melodic 7bulls repository</name>
            <url>https://nexus.7bulls.eu:8443/repository/maven-snapshots/</url>
        </snapshotRepository>
    </distributionManagement>

    <scm>
        <connection>scm:git:https://bitbucket.7bulls.eu/scm/mel/upperware.git</connection>
        <developerConnection>scm:git:https://bitbucket.7bulls.eu/scm/mel/upperware.git</developerConnection>
    </scm>

    <profiles>
        <profile>
            <id>without-docker</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>com.spotify</groupId>
                        <artifactId>docker-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>execution</id>
                                <phase>none</phase>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>docker-remote</id>
            <properties>
                <docker.repository>gitlab.ow2.org:4567/</docker.repository>
                <docker.push>true</docker.push>
            </properties>
        </profile>

    </profiles>

</project>
