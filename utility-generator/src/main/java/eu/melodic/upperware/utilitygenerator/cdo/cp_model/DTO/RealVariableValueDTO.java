/* * Copyright (C) 2018 7bulls.com
 *
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */

package eu.melodic.upperware.utilitygenerator.cdo.cp_model.DTO;

public class RealVariableValueDTO extends VariableValueDTO<Double> {

    public RealVariableValueDTO(String name, Double value) {
        super(name, value);
    }
}
