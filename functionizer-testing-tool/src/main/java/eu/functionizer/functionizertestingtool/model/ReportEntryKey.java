package eu.functionizer.functionizertestingtool.model;

public class ReportEntryKey {
    public final static String EVENT = "event";
    public final static String EXPECTED_VALUE = "expectedValue";
    public final static String CONDITION = "condition";
    public final static String ACTUAL_OUTPUT = "actualOutput";
    public final static String IGNORED = "ignored";
    public final static String ROOT = "root";
    public final static String STAGE = "stage";
    public final static String FAILURE_CAUSE = "failureCause";
}
