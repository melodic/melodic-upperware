package eu.paasage.upperware.profiler.generator.communication;

import camel.core.CamelModel;
import eu.paasage.mddb.cdo.client.exp.CDOSessionX;
import eu.paasage.upperware.metamodel.cp.ConstraintProblem;
import org.eclipse.emf.cdo.transaction.CDOTransaction;

public interface CdoService {

    CamelModel getCamelModel(String name, CDOTransaction tr);

    CDOSessionX openSession();

    void saveModels(ConstraintProblem cp, CDOSessionX cdoSessionX);
}
