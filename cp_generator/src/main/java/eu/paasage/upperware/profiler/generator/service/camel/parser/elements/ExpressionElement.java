package eu.paasage.upperware.profiler.generator.service.camel.parser.elements;

public interface ExpressionElement {
    String getLooksLike();
    int getTypeId();
}
