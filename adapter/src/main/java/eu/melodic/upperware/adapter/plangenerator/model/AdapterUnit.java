package eu.melodic.upperware.adapter.plangenerator.model;

public enum AdapterUnit {

    DAYS, HOURS, MICROSECONDS, MILLISECONDS, MINUTES, NANOSECONDS, SECONDS
}
