package eu.melodic.upperware.adapter.plangenerator.model;

public enum AdapterTaskType {
    BATCH,
    SERVICE;
}
