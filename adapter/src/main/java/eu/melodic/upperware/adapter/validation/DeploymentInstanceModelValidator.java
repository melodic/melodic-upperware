package eu.melodic.upperware.adapter.validation;

import camel.deployment.DeploymentInstanceModel;

public interface DeploymentInstanceModelValidator extends Validator<DeploymentInstanceModel> {
}
