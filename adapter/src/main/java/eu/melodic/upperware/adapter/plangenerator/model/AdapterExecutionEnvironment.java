package eu.melodic.upperware.adapter.plangenerator.model;

public enum AdapterExecutionEnvironment {
    SPARK,
    NATIVE,
    CONTAINER,
    LANCE;
}
