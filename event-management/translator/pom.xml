<!--
  ~ Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ This Source Code Form is subject to the terms of the Mozilla Public License, v2.0, unless
  ~ Esper library is used, in which case it is subject to the terms of General Public License v2.0.
  ~ If a copy of the MPL was not distributed with this file, you can obtain one at
  ~ https://www.mozilla.org/en-US/MPL/2.0/
  -->
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>eu.melodic.event</groupId>
        <artifactId>event-management</artifactId>
        <version>3.1.0-SNAPSHOT</version>
    </parent>

    <artifactId>translator</artifactId>
    <name>Upperware - EMS - CAMEL-to-EPL translator</name>

    <properties>
        <!-- Dependencies versions -->
        <jgrapht.version>1.2.1-SNAPSHOT</jgrapht.version>
        <graphviz-java.version>0.5.4</graphviz-java.version>
        <hibernate-validator.version>6.0.16.Final</hibernate-validator.version>

        <!-- Required in order to compile with Thymeleaf -->
        <thymeleaf.version>3.0.11.RELEASE</thymeleaf.version>
        <thymeleaf-layout-dialect.version>2.3.0</thymeleaf-layout-dialect.version>
    </properties>

    <!-- repository for JGraphT -->
    <repositories>
        <repository>
            <id>maven-snapshots</id>
            <url>http://oss.sonatype.org/content/repositories/snapshots</url>
            <layout>default</layout>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

    <dependencies>
        <!-- PaaSage and Melodic dependencies -->
        <dependency>
            <groupId>eu.melodic.cdo</groupId>
            <artifactId>client</artifactId>
            <classifier>repackaged</classifier>
        </dependency>
        <dependency>
            <groupId>org.ow2.paasage</groupId>
            <artifactId>melodic-commons</artifactId>
            <version>${melodic.version}</version>
        </dependency>
        <dependency>
            <groupId>org.ow2.paasage</groupId>
            <artifactId>upperware-metamodel</artifactId>
            <version>${melodic.version}</version>
        </dependency>
        <dependency>
            <groupId>camel</groupId>
            <artifactId>camel</artifactId>
            <version>${melodic.version}</version>
        </dependency>
        <dependency>
            <groupId>camel</groupId>
            <artifactId>camel.dsl</artifactId>
            <version>${melodic.version}</version>
        </dependency>
        <dependency>
            <groupId>eu.melodic</groupId>
            <artifactId>interfaces</artifactId>
        </dependency>
        <dependency>
            <groupId>eu.melodic.event</groupId>
            <artifactId>broker-cep</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- Spring-boot dependencies -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.hibernate/hibernate-validator -->
        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>${hibernate-validator.version}</version>
        </dependency>

        <dependency>
            <groupId>org.thymeleaf</groupId>
            <artifactId>thymeleaf-spring5</artifactId>
            <version>${thymeleaf.version}</version>
        </dependency>

        <!-- Lombok project -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- JGraphT graph library (https://jgrapht.org/) -->
        <dependency>
            <groupId>org.jgrapht</groupId>
            <artifactId>jgrapht-core</artifactId>
            <version>${jgrapht.version}</version>
        </dependency>
        <dependency>
            <groupId>org.jgrapht</groupId>
            <artifactId>jgrapht-io</artifactId>
            <version>${jgrapht.version}</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/guru.nidi/graphviz-java -->
        <dependency>
            <groupId>guru.nidi</groupId>
            <artifactId>graphviz-java</artifactId>
            <version>${graphviz-java.version}</version>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.8.5</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-collections4 -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
            <version>4.2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-lang3 -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.8.1</version>
        </dependency>
    </dependencies>

</project>
