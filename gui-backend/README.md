###### Using locally

For using GuiBackendApplication locally, in case if you don't have certificate for Mule, you need to **disable SSL verification** in 
${MELODIC_CONFIG_DIR}/eu.melodic.upperware.guiBackend.properties file by **setting esb.sslVerificationEnabled parameter as false**.   
