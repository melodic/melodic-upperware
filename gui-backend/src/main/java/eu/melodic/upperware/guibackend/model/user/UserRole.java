package eu.melodic.upperware.guibackend.model.user;

public enum UserRole {
    ADMIN, USER, TECHNICAL_USER
}
