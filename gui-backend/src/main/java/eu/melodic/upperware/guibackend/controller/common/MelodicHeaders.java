package eu.melodic.upperware.guibackend.controller.common;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public final class MelodicHeaders {

    public final static String REFRESH = "Refresh";

}
