package eu.melodic.upperware.guibackend.controller.common;

public enum ProcessState {
    STARTED, FINISHED, UNKNOWN
}
