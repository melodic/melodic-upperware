package eu.melodic.upperware.guibackend.service.cdo;

import java.io.File;

public interface ModelNameGenerator {

    String getModelName(File modelFile);
}
