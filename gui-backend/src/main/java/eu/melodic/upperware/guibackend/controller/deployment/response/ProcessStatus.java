package eu.melodic.upperware.guibackend.controller.deployment.response;

public enum ProcessStatus {
    SUCCESS, ERROR
}
