package eu.melodic.upperware.guibackend.controller.common;

public enum VariableStatus {
    SUCCESS, ERROR, UNKNOWN, ACTIVE
}
