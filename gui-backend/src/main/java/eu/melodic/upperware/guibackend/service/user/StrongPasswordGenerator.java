package eu.melodic.upperware.guibackend.service.user;

public interface StrongPasswordGenerator {

    String generatePassword();
}
