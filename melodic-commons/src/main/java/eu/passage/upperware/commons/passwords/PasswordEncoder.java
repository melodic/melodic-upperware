package eu.passage.upperware.commons.passwords;

public interface PasswordEncoder {

    String encode(String password);

}
