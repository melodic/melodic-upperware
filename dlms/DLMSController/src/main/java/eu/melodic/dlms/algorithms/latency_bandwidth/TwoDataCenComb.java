package eu.melodic.dlms.algorithms.latency_bandwidth;

import lombok.Data;

@Data
public class TwoDataCenComb {
	private final String dc1Id;
	private final String dc2Id;
}
