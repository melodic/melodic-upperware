/*
 * Copyright (C) 2017 Institute of Communication and Computer Systems (imu.iccs.com)
 *
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */

package eu.melodic.dlms.metric.receiver.metricvalue;

import lombok.Getter;

@Getter
//@ToString(doNotUseGetters = true, exclude = { "host_name", /* "component_name", */ "level" })
public class MetricValueEventAcDsDataRead {
	private String ac;
	private String ds;
	private long amountRead;
	private long timeStamp;
}
