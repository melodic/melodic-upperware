/*
 * Melodic EU Project
 * DLMS WebService
 * Data Source Types
 * @author: ferox
 */

package eu.melodic.upperware.dlms;

/**
 * Represents a type of datasource.
 */
public enum DataSourceType {
		HDFS,S3,MYSQL
		// other datasources needs to be added as they are supported in the future
}
