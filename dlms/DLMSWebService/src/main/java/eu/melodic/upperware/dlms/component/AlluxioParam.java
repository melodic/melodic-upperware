package eu.melodic.upperware.dlms.component;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AlluxioParam {

	private String command;

}
