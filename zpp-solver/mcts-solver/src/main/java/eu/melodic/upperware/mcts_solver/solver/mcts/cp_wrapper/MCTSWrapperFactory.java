package eu.melodic.upperware.mcts_solver.solver.mcts.cp_wrapper;

public interface MCTSWrapperFactory {
    MCTSWrapper create();
}
