package eu.melodic.upperware.mcts_solver.solver.mcts.tree_impl.policy;

public enum AvailablePolicies {
    RANDOM_POLICY,
    CHEAPEST_POLICY
}
