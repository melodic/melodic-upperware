package eu.melodic.upperware.cp_wrapper.utils.cp_variable;

public enum VariableNumericType {
    INT,
    DOUBLE
}
