package eu.melodic.upperware.cp_wrapper.utility_provider;

public interface UtilityProviderFactory {
    UtilityProvider create();
}
