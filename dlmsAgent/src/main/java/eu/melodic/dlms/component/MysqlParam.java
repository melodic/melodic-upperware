package eu.melodic.dlms.component;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MysqlParam {

	private String port;
	private String userName;
	private String password;

}
