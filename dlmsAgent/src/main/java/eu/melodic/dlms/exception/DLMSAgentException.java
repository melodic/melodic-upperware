package eu.melodic.dlms.exception;

public class DLMSAgentException extends RuntimeException {

    public DLMSAgentException(String message) {
        super(message);
    }
}
