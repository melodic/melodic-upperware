/*
 * Copyright (C) 2017-2020 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.penaltycalculator;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

//import org.apache.commons.*;
//import eu.melodic.cloudiator.client.model.NodeCandidate;
//import eu.melodic.vassilis.staff.PenaltyFunction.ConfigurationElement;
//import org.apache.commons.*;

@SpringBootApplication
@EnableConfigurationProperties
@Slf4j
public class PenaltyTests implements CommandLineRunner {
    @Autowired
    private PenaltyFunction penaltyCalculator;
    @Autowired
    private PenaltyFunctionProperties properties;

    public static void main(String[] args) {
        log.info("STARTING THE APPLICATION");
        SpringApplication.run(PenaltyTests.class, args);
        log.info("APPLICATION FINISHED");
    }

    public void run(String... args) throws Exception {
        Collection<PenaltyConfigurationElement> collection_1 = readConfigElementsFromFile(args[0]);
        log.debug("Collection-1:\n{}", PenaltyFunction.toString(collection_1));

        Collection<PenaltyConfigurationElement> collection_2 = readConfigElementsFromFile(args[1]);
        log.debug("Collection-2:\n{}", PenaltyFunction.toString(collection_2));

        PenaltyFunctionResult penaltyResults = penaltyCalculator.evaluatePenaltyFunction(collection_1, collection_2);

        //normalized average startup time using max-min normalization
        log.info("Average Time of VM and Component Startup Time : {}", penaltyResults.getStartupTime());
        log.info("Normalized Average Time of VM and Component Startup Time : {}", penaltyResults.getPenaltyValue());
    }

    protected Collection<PenaltyConfigurationElement> readConfigElementsFromFile(String fileName) throws IOException {
        try (Reader reader = new FileReader(fileName)) {
            Type listType = new TypeToken<ArrayList<PenaltyConfigurationElement>>() {}.getType();
            return new Gson().fromJson(reader, listType);
        }
    }
}