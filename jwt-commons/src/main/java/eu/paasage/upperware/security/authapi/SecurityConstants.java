package eu.paasage.upperware.security.authapi;

public class SecurityConstants {
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String REFRESH_HEADER_STRING = "Refresh";

	public static final String AUDIENCE_UPPERWARE = "UPPERWARE";
	public static final String AUDIENCE_JWT = "JWT_SERVER";

	public static final String MELODIC_SECURITY_ENABLED_PROPERTY = "melodic.security.enabled";
}
