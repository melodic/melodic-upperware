/**
 */
package eu.paasage.upperware.metamodel.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Numeric Value Upperware</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.paasage.upperware.metamodel.types.TypesPackage#getNumericValueUpperware()
 * @model abstract="true"
 * @generated
 */
public interface NumericValueUpperware extends ValueUpperware {
} // NumericValueUpperware
