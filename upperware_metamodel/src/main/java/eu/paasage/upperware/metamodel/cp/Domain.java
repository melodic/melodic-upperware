/**
 */
package eu.paasage.upperware.metamodel.cp;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.paasage.upperware.metamodel.cp.CpPackage#getDomain()
 * @model abstract="true"
 * @extends CDOObject
 * @generated
 */
public interface Domain extends CDOObject {
} // Domain
