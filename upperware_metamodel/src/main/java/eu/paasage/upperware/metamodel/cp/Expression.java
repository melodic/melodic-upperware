/**
 */
package eu.paasage.upperware.metamodel.cp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.paasage.upperware.metamodel.cp.CpPackage#getExpression()
 * @model abstract="true"
 * @generated
 */
public interface Expression extends CPElement {
} // Expression
