/**
 */
package eu.paasage.upperware.metamodel.types;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Upperware</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.paasage.upperware.metamodel.types.TypesPackage#getValueUpperware()
 * @model abstract="true"
 * @extends CDOObject
 * @generated
 */
public interface ValueUpperware extends CDOObject {
} // ValueUpperware
